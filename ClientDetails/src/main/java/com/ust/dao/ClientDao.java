package com.ust.dao;

import java.util.Map;

import com.ust.model.Client;

public interface ClientDao {
	
	boolean insertClientDetails(String 	passportNumber,Client client);	
	
	Map<String, Client> getAllClientDetails();

}

